
PREFIX ?= /usr/local
BINDIR = $(DESTDIR)$(PREFIX)/bin
LIBDIR = $(DESTDIR)$(PREFIX)/lib/ansible-view-vault

.PHONY: test install

test:
	@PATH="./bin:${PATH}" bats -r test/

install:
	install -d -m 775 $(BINDIR) $(LIBDIR)
	install -m 775 bin/ansible-view-vault $(BINDIR)
	install -m 664 lib/view-partial.mawk $(LIBDIR)
