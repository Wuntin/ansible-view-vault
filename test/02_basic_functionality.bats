#!/usr/bin/env bats

setup() {
  echo 12345 > default
  export ANSIBLE_VAULT_IDENTITY_LIST=default
}

teardown() {
    rm default
}

@test "test fully encrypted file" {
    run ansible-view-vault test/basic/fully_encrypted.yml

    [ "$status" -eq 0 ]
    [ "$output" = "$(cat test/basic/original.yml)" ]
}

@test "test partially encrypted file" {
    run ansible-view-vault test/basic/partially_encrypted.yml

    [ "$status" -eq 0 ]
    [ "$output" = "$(cat test/basic/original.yml)" ]
}
