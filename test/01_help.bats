#!/usr/bin/env bats

@test "test -h parameter" {
  run ansible-view-vault -h

  [ "$status" -eq 1 ]
  [ "${output:0:25}" = "Usage: ansible-view-vault" ]
}
