BEGIN {
  view_command = ansible_vault " view " temp_file
}

END {
  if (started) {
    view_block()
  }

  if (potential) {
    print rows[0]
  }
}

{
  if (started) {
    if (/^ +[0-9a-f]+$/) {
      rows[row_count] = $0
      row_count++
    } else {
      started = 0

      view_block()
    }
  }

  if (potential) {
    if ($0 ~ vault_pattern) {
      potential = 0
      started = 1

      rows[1] = $0
      row_count = 2
    } else {
      potential = 0

      print rows[0]
    }
  }

  if (!started && !potential) {
    if (/^ *[a-zA-Z_-]+: !vault \|$/) {
      potential = 1

      rows[0] = $0
    } else {
      print
    }
  }
}

function view_block() {
  for (i = 1; i < row_count; i++) {
    sub("^ +", "", rows[i])

    print rows[i] > temp_file
  }
  close(temp_file)

  row_count = 1

  while (result = view_command | getline row > 0) {
    sub("^", "          ", row)

    rows[row_count] = row
    row_count++
  }

  result = close(view_command)

  if (result != 0) {
    exit 1
  }

  sub(":.*$", ": ", rows[0])
  sub("^ +", rows[0], rows[1])

  for (i = 1; i < row_count; i++) {
    print rows[i]
  }
}